@default_files = ( 'main.tex' );
$pdf_mode=1;
push @generated_exts, 'mw', 'gnt', 'lms', 'lse', 'ldl', 'ltk', 'lwt', 'lrk', 'brf';
$cleanup_includes_generated = 1;
$cleanup_includes_cusdep_generated = 1;
$clean_full_ext .= " %R-blx.bib bbl %R.run.xml mw gnt lms lse ldl ltk lwp lrk brf";
